package handlers

import (
	"database/sql"
	"net/http"
	"simpleapi/models"

	"github.com/labstack/echo"
)

//type H map[string]interface{}

func GetHardwares(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		pid := c.Param("*")
		return c.JSON(http.StatusOK, models.GetHardwares(db, pid))
	}
}

func GetSoftwares(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		pid := c.Param("pid")
		return c.JSON(http.StatusOK, models.GetSoftwares(db, pid))
	}
}

func GetLogs(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		pid := c.Param("pid")
		return c.JSON(http.StatusOK, models.GetLogs(db, pid))
	}
}
