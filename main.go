package main

import (
	"database/sql"
	"fmt"
	"simpleapi/handlers"

	"github.com/labstack/echo"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	e := echo.New()
	db, err := sql.Open("mysql", "root:dispusip2023@tcp(localhost:3306)/db_silaban")
	// db, err := sql.Open("mysql", "root:N3t0prmgr!@tcp(localhost:3306)/db_portal")
	if err != nil {
		fmt.Println("error")
	}

	e.GET("/api/v1/resources/hardware/*", handlers.GetHardwares(db))
	e.GET("/api/v1/resources/software/:pid", handlers.GetSoftwares(db))
	e.GET("/api/v1/resources/log/:pid", handlers.GetLogs(db))

	e.Logger.Fatal(e.Start(":5012"))
}
