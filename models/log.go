package models

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

type Log struct {
	ID            int    `json:"id"`
	NameLog       string `json:"name_log"`
	Explanation   string `json:"explanation"`
	Workaround    string `json:"workaround"`
	DateGenerated string `json:"date_generated"`
	GeneratedBy   string `json:"generated_by"`
	Technology    string `json:"technology"`
}

type LogCollection struct {
	Logs []Log
}

func GetLogs(db *sql.DB, pid string) []Log {
	sql := "SELECT * FROM log_log where  name_log like '%" + pid + "%'"
	rows, err := db.Query(sql)

	if err != nil {
		panic(err)
	}

	defer rows.Close()

	result := LogCollection{}
	for rows.Next() {
		log := Log{}
		err2 := rows.Scan(&log.ID, &log.NameLog, &log.Explanation, &log.Workaround, &log.DateGenerated, &log.GeneratedBy, &log.Technology)
		if err2 != nil {
			panic(err2)
		}
		result.Logs = append(result.Logs, log)
	}
	resultLog := result.Logs
	return resultLog
}
