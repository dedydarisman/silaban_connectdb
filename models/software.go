package models

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

type Software struct {
	ID                int    `json:"id"`
	SoftwareVersion   string `json:"software_version"`
	EOSMaintenance    string `json:"eos_maintenance"`
	LastDateOfSupport string `json:"last_day_of_support"`
	DateGenerated     string `json:"date_generated"`
	GeneratedBy       string `json:"generated_by"`
	Technology        string `json:"technology"`
}

type SoftwareCollection struct {
	Softwares []Software
}

func GetSoftwares(db *sql.DB, pid string) []Software {
	sql := "SELECT * FROM software_software where software_version = '" + pid + "'"
	rows, err := db.Query(sql)

	if err != nil {
		panic(err)
	}

	defer rows.Close()

	result := SoftwareCollection{}
	for rows.Next() {
		software := Software{}
		err2 := rows.Scan(&software.ID, &software.SoftwareVersion, &software.EOSMaintenance, &software.LastDateOfSupport, &software.DateGenerated, &software.GeneratedBy, &software.Technology)
		if err2 != nil {
			panic(err2)
		}
		result.Softwares = append(result.Softwares, software)
	}
	resultSoftware := result.Softwares
	return resultSoftware
}
