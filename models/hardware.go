package models

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

type Hardware struct {
	ID            int    `json:"id"`
	DevicePID     string `json:"device"`
	EndOfSupport  string `json:"date"`
	DateGenerated string `json:"generated_date"`
	GeneratedBy   string `json:"generated_by"`
	Technology    string `json:"technology"`
}

type HardwareCollection struct {
	Hardwares []Hardware //`json:"items"`
}

func GetHardwares(db *sql.DB, pid string) []Hardware {
	sql := "SELECT * FROM hardware_hardware where device_pid = '" + pid + "'"
	rows, err := db.Query(sql)

	if err != nil {
		panic(err)
	}

	defer rows.Close()

	result := HardwareCollection{}
	for rows.Next() {
		hardware := Hardware{}
		err2 := rows.Scan(&hardware.ID, &hardware.DevicePID, &hardware.EndOfSupport, &hardware.DateGenerated, &hardware.GeneratedBy, &hardware.Technology)
		if err2 != nil {
			panic(err2)
		}
		result.Hardwares = append(result.Hardwares, hardware)
	}
	resultHardware := result.Hardwares
	return resultHardware
}
